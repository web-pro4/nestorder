import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(1)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
