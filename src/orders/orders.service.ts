import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { OrderItem } from './entities/order-item';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersrepository: Repository<Order>,
    @InjectRepository(Customer)
    private customersrepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsrepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsrepository: Repository<OrderItem>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const customer = await this.customersrepository.findOneBy({
      id: createOrderDto.customerId,
    });
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.ordersrepository.save(order);

    const orderItems: OrderItem[] = await Promise.all(
      createOrderDto.orderItems.map(async (od) => {
        const orderItem = new OrderItem();
        orderItem.amount = od.amount;
        orderItem.product = await this.productsrepository.findOneBy({
          id: od.productId,
        });
        orderItem.name = orderItem.product.name;
        orderItem.price = orderItem.product.price;
        orderItem.total = orderItem.price * orderItem.amount;
        orderItem.order = order;
        return orderItem;
      }),
    );
    for (const orderItem of orderItems) {
      this.orderItemsrepository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }
    await this.ordersrepository.save(order);
    return await this.ordersrepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersrepository.find({
      relations: ['customer', 'orderItems'],
    });
  }

  findOne(id: number) {
    return this.ordersrepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.ordersrepository.findOneBy({ id: id });
    return this.ordersrepository.softRemove(order);
  }
}
