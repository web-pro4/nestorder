import { Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsArray,
  ValidateNested,
  IsPositive,
} from 'class-validator';
class CreatedOrderItemDto {
  @IsNotEmpty()
  @IsPositive()
  productId: number;
  @IsNotEmpty()
  @IsPositive()
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  @IsPositive()
  customerId: number;

  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreatedOrderItemDto)
  orderItems: CreatedOrderItemDto[];
}
