import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @MinLength(1)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @MinLength(10)
  tel: string;

  @IsNotEmpty()
  @MinLength(1)
  gender: string;
}
